trigger ADT2018_Case_Trigger on Case (after insert, before update, after update)
{
    TriggerHandler TH = new TriggerHandler(Trigger.new, Trigger.oldMap, Trigger.isInsert);
	TH.Run();
}