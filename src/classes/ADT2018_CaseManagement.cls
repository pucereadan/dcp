public class ADT2018_CaseManagement
{
	public static void generateTaskAfterCaseCreation(List<Case> myNewCases, Map<Id, Case> oldHighCases, Boolean isInsert)
    {
        List<Task> taskToBeCreated = new List<Task>();
        List<FeedItem> feedItemsToBeCreated = new List<FeedItem>();
        
        for(Case c: myNewCases)
        {
            
            if(isInsert)
            {
                Task firstTask = new Task();
                
                firstTask.Subject = 'Contact the Customer';
                firstTask.WhatId = c.Id;
                firstTask.OwnerId = c.OwnerId;
                firstTask.WhoId = c.ContactId;
                firstTask.ActivityDate = (Datetime.now().AddDays(7)).date();
				taskToBeCreated.add(firstTask);
            }
            
            if((c.Priority == 'High' && oldHighCases != null && c.Priority != oldHighCases.get(c.Id).Priority)|| (c.Priority == 'High' && isInsert))
            {
                Task secondTask = new Task();
                Task task4 = new Task();
                Task task3 = new Task();
                secondTask.Subject = 'Do a commercial offer';
                secondTask.WhatId = c.Id;
                secondTask.OwnerId = c.Account_Owner_ID__c;
                secondTask.WhoId = c.ContactId;
                secondTask.ActivityDate = (Datetime.now().AddDays(7)).date();
            	taskToBeCreated.add(secondTask);
                System.Debug('TVC second task added: ' + secondTask);
            }
            
        	FeedItem myFeed = new FeedItem();
            myFeed.ParentId = c.Account_Owner_ID__c;
            myFeed.Body = 'A new task related to Case Number ' + c.CaseNumber + ' was assigned to you. Please ensure you follow-up closely.';
            
            feedItemsToBeCreated.add(myFeed);
            
            System.Debug('TVC taskToBeCreated: ' + taskToBeCreated);
        }
        if(!taskToBeCreated.isEmpty())
        {
            insert taskToBeCreated;
            System.Debug('TVC inserted taskToBeCreated: ' + taskToBeCreated);

        }
        if(!feedItemsToBeCreated.isEmpty())
        {
            insert feedItemsToBeCreated;
        }
    }
    
    public static void openTaskCheck(List<Case> allCasesList) {
        
        List<Task> openTasks = [SELECT id, whatId 
                                FROM Task 
                                WHERE Status 
                                IN ('Not Started', 'In Progress', 'Waiting on someone else') 
                                AND WhatId in: allCasesList];
        
        if(openTasks.isEmpty())
        {
            Set<ID> casesWithOpenTasks = new Set<ID>();
        
            for(Task openTask: openTasks)
            {
                if(casesWithOpenTasks.contains(openTask.whatId))
                {
                    casesWithOpenTasks.add(openTask.whatId);
                }
                
                for(Case updatedCase: allCasesList)
                {
                    if(casesWithOpenTasks.contains(updatedCase.Id))
                    {
                        updatedCase.addError('You can only close a Case when all related Tasks are completed or deferred.');
                    }
                }
            }
        }
    }
}