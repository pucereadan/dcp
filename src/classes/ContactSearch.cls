public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName, String PostalCode) {
        List<Contact> contacts = [SELECT Id, Name FROM Contact WHERE LastName=: lastName and MailingPostalCode =: PostalCode];
        return contacts;
    }
}