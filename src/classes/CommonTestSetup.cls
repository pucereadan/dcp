@isTest
public class CommonTestSetup {
    
    private static Case testCase = new Case();
    private static Account testAccount = new Account();
	private static Contact testContact = new Contact();
    
    @testSetup static void setup()
    {
        testAccount.Name = 'testAccount';
        testContact.lastName = 'testName';
        testContact.AccountId = testAccount.Id;
        
        insert testAccount;
		
        insert testContact;
        testCase = new Case(AccountId = testAccount.Id , ContactId = testContact.AccountId , Subject  = 'TestCase', Status = 'New', Origin = 'Web');
    }
    
    @isTest static void taskUponCreationHighPriority()
    {
        setup();
        //Start Test
        test.startTest();

        insert testCase;
        testCase.Priority = 'High';
        update testCase;
        
        testCase = [SELECT Priority FROM Case WHERE Id =: testCase.Id];
        
        System.debug('PD - testCase result ' + testCase);
        
        //Stop Test
        test.stopTest();
       
        List<Task> testTasks = [SELECT Id FROM Task WHERE WhatId =: testCase.Id];
        
        System.assertEquals(2, testTasks.size());
    }
    
    @isTest static void taskUponCreationNonHighPriority() 
    {
        setup();
        //Start Test
        test.startTest();
        insert testCase;
        
        //Stop Test
        test.stopTest();
        
        List<Task> testTasks = [SELECT Id FROM Task WHERE WhatId =: testCase.Id];
        
        System.assertEquals(1, testTasks.size());
    }
}