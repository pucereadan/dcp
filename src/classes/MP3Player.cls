public class MP3Player {
    
    public String song
    {
        get
        {
            if(song == NULL)
            {
                return 'No song selected';
            }
            else
            {
                return song;
            }
        }
        private set;
    }
    
	public void playSong(String songName)
    {
        song = songName;
    }
    
    public void stopSong()
    {
        song = NULL;
    }
}