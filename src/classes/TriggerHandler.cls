public class TriggerHandler {
	List<Case> newValueOfCases;
    Map<Id, Case> oldValueOfCases;
    boolean isInsert;
    Map<String, Case> statusRelatedToCases;
    
    public TriggerHandler(List<Case> newValueOfCases, Map<Id, Case> oldValueOfCases, boolean isInsert)
    {
        this.newValueOfCases = newValueOfCases;
        this.isInsert = isInsert;
        
        if(isInsert == false)
        {
            this.oldValueOfCases = oldValueOfCases;
        }
    }
    public void Run()
    {
        if(Trigger.isBefore)
        {
            if(Trigger.isUpdate)
            {
                ADT2018_CaseManagement.openTaskCheck(newValueOfCases);
            }
        }
        
        if(Trigger.isAfter)
        {
            if(Trigger.isInsert || Trigger.isUpdate)
            {
                ADT2018_CaseManagement.generateTaskAfterCaseCreation(newValueOfCases, oldValueOfCases, isInsert);
            }
        }
    }
}